#!/bin/bash
cd ~
if [ -f "$HOME/.git-aliases" ]; then
    rm $HOME/.git-aliases
fi
wget https://bitbucket.org/redjohn360/linux-config/raw/master/.git-aliases
gcheck;
if [ $? -ne 0 ]
then
    echo -e "\nif [ -f \"\$HOME/.git-aliases\" ]; then\n\t. \$HOME/.git-aliases\nfi" >> ~/.bashrc
fi
source ~/.bashrc