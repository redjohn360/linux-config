# Installation
Dies einfach in die Konsole kopieren aus ausführen.
```
wget -O -  https://bitbucket.org/redjohn360/linux-config/raw/master/linux-config.sh | bash
```

# Git-Kurzbefehle: Cheat Sheet

## Simple Kurzformen
- `g` : `git`
- `gs` :`git status`
- `gc` : `git commit`
- `gp` : `git push`
- `gpll` : `git pull`
- `gf` : `git fetch`

## Commits
- `g.` : Hinzufügen aller Änderungen zur Changelist ( `git add .` )
- `gmom` : Merge origin master ( `gf && git merge origin/master` )
- `gfx` : Fix - Alle Änderungen zur Changelist hinzufügen und an den letzten commit anhängen ( `rm -f .git/index.lock && git add . && git commit --amend --no-edit` )
- `gfxp` : Fix Push - Push mit "force with lease" tag ( `git push --force-with-lease` )
- `gfp` : Fetch Prune - Entfent alle nicht mehr existenten remote branches ( `git fetch --prune` )

## Branches
- `gbm` : Branch Master ( `git checkout master` )
- `glb` : List Branches - Auflistung aller Branches lokal und remote ( `git branch --all` )
- `gb $branchname` : Branch - Wechseln zum Branch $branchname, dabei reicht auch der unvollständige aber eindeutige Branchname

## Misc
- `gcl`: Clear - Löschen aller Lokalen Branchen, zu denen kein Remote Branch mehr vorhanden ist
- `brc`: Bashrc - Öffnet die .bashrc und aktualisiert diese als Source in der aktuellen Konsole

## Statistika
- `gscore` : Score - Tabelle mit Anzahl der Commits pro Entwickler ( `git shortlog -s -n --all --no-merges` )
- `gscorel` : Score Lines - Tabelle mit Anzahl der Commiteten Lines pro Entwickler ( `git ls-files | while read f; do git blame --line-porcelain $f | grep "^author "; done | sort -f | uniq -ic | sort -n` )
- `glines`: Lines - Anzahl aller Versionierten Lines des Repos ( `git ls-files | xargs cat | wc -l` )
- `gtime $dev`: Time - Auflistung der Häufigkeit der Commitzeitpunkte des Entwicklers $dev